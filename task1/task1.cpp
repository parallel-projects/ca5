#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "pthread.h"
#include <sys/time.h>
#define     VECTOR_SIZE     1048576
#define     NUM_THREADS     6

typedef struct
{
    int start_index;
    int end_index;

} in_param_t;

float floatVec[VECTOR_SIZE];
int parallel_index;
float parallel_max;

pthread_mutex_t lock;
int ret = pthread_mutex_init(&lock, NULL);


void *find_local_max(void *arg)
{
    in_param_t * inp = (in_param_t *) arg;
    int local_index;
    float local_max = 0;
    float current_element;

    for(int i = inp->start_index; i < inp->end_index; i++)
    {
        current_element = floatVec[i];
        if (current_element >= local_max)
        {
            local_max = current_element;
            local_index = i;
        }
    }

    pthread_mutex_lock(&lock);
    if(local_max >= parallel_max)
    {
        parallel_max = local_max;
        parallel_index = local_index;
    }
    pthread_mutex_unlock(&lock);
    pthread_exit(NULL);
}


int main (void)
{
    printf("Student IDs:\n810196518\n810196562\n\n");

    srand((unsigned int)time(NULL));
    
    for (int i = 0; i < VECTOR_SIZE; i++)
        floatVec[i] = float(rand())/float((RAND_MAX)) * 100.0;
    
    struct timeval serial_start, serial_end, parallel_start, parallel_end;
    
    int index;
    float max;
    float current_element;
    
    // Serial implementation
    gettimeofday(&serial_start, NULL);
    
    index = 0;
    max = 0.0; 
    
    for (int i = 0; i < VECTOR_SIZE; i++){
        current_element = floatVec[i];
        if (current_element >= max){
            max = current_element;
            index = i;
        }
    }
    
    gettimeofday(&serial_end, NULL);
    printf("Serial result:\n");
    printf("max number is: %f at %dth index\n", max, index);
    long serial_seconds = (serial_end.tv_sec - serial_start.tv_sec);
    long serial_micros = ((serial_seconds * 1000000) + serial_end.tv_usec) - (serial_start.tv_usec);
    printf("serial executaion time is %ld s and %ld micros\n\n", serial_seconds, serial_micros);


    // Parallel implementation
    gettimeofday(&parallel_start, NULL);

    pthread_t th[NUM_THREADS];
    in_param_t in_param [NUM_THREADS] =
            {{0, VECTOR_SIZE/6},
            {VECTOR_SIZE/6, VECTOR_SIZE/3},
            {VECTOR_SIZE/3, VECTOR_SIZE/2},
            {VECTOR_SIZE/2, 2 * VECTOR_SIZE/3},
            {2 * VECTOR_SIZE/3, 5 * VECTOR_SIZE/6},
            {5 * VECTOR_SIZE/6, VECTOR_SIZE}};
    
    for(int i = 0; i < NUM_THREADS; i++)
        pthread_create(&th[i], NULL, find_local_max, (void *) &in_param[i]);
    
    for(int i = 0; i < NUM_THREADS; i++)
        pthread_join(th[i], NULL);
    
    pthread_mutex_destroy(&lock);
    
    gettimeofday(&parallel_end, NULL);
    printf("Parallel result:\n");
    printf("max number is: %f at %dth index\n", parallel_max, int(parallel_index));
    long parallel_seconds = (parallel_end.tv_sec - parallel_start.tv_sec);
    long parallel_micros = ((parallel_seconds * 1000000) + parallel_end.tv_usec) - (parallel_start.tv_usec);
    printf("parallel executaion time is %ld s and %ld micros\n\n", parallel_seconds, parallel_micros);
    printf("Speed up: %f\n", (float)serial_micros/(float)parallel_micros);

    pthread_exit(0);
}