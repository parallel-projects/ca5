#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#define     VECTOR_SIZE     1048576
#define THREAD_NUM 8

void serialQuickSort(float arr[], int low, int high);
void *parallelQuicksort(void *threadarg);
typedef struct{
    float *vec;
    int low;
    int high;
    int thread_level;
}thread_data;


int main (int argc, char *argv[])
{
    printf("Student IDs:\n810196518\n810196562\n\n");
    float *floatVec;
    float *floatVecCopy;
    floatVec = new float [VECTOR_SIZE];
    floatVecCopy = new float [VECTOR_SIZE];
    srand((unsigned int)time(NULL));
    float cur;
    for (int i = 0; i < VECTOR_SIZE; i++){
        cur = float(rand())/float((RAND_MAX)) * 100.0;
        floatVec[i] = cur;
        floatVecCopy[i] = cur;
    }
    struct timeval start, end;


    //serial implementation
    gettimeofday(&start, NULL);
    serialQuickSort(floatVec, 0, VECTOR_SIZE-1);
    gettimeofday(&end, NULL);

    printf("Serial result:\n");
    long serial_seconds = (end.tv_sec - start.tv_sec);
    long serial_micros = ((serial_seconds * 1000000) + end.tv_usec) - (start.tv_usec);
    printf("serial executaion time is %ld s and %ld micros\n\n", serial_seconds, serial_micros);

    //parallel implementation
    
    gettimeofday(&start, NULL);
    thread_data td = {floatVecCopy, 0, VECTOR_SIZE-1, THREAD_NUM};
    pthread_t theThread;
    pthread_create(&theThread, NULL, parallelQuicksort,&td);

    pthread_join(theThread, NULL);

    gettimeofday(&end, NULL);

    
    printf("Parallel result:\n");
    long parallel_seconds = (end.tv_sec - start.tv_sec);
    long parallel_micros = ((parallel_seconds * 1000000) + end.tv_usec) - (start.tv_usec);
    printf("parallel executaion time is %ld s and %ld micros\n\n", parallel_seconds, parallel_micros);
    printf("Speed up: %f\n", (float)serial_micros/(float)parallel_micros);
    
    
    for (int i=1; i<VECTOR_SIZE;i++){
        if (floatVecCopy[i]<floatVecCopy[i-1]){
            printf("parallel not sorted!!\n");
        }
        if (floatVec[i]<floatVec[i-1]){
            printf("serial not sorted!!\n");
        }
    }
    
    free(floatVec);
    free(floatVecCopy);
    pthread_exit(NULL);
}


void serialQuickSort(float arr[], int low, int high)
{
    if (low < high)
    {
        float pivot = arr[high];
        int i = (low - 1);
        float temp;
        for (int j = low; j <= high- 1; j++)
        {
            if (arr[j] < pivot)
            {
                i++;
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        temp = arr[i+1];
        arr[i+1] = arr[high];
        arr[high] = temp;

        int p = i+1;
        serialQuickSort(arr, low, p - 1);
        serialQuickSort(arr, p + 1, high);
    }
}

void *parallelQuicksort(void *threadarg)
{
    int t;

    thread_data *my_data = (thread_data *)threadarg;
    if (my_data->thread_level <= 0 || my_data->low == my_data->high)
    {
        serialQuickSort(my_data->vec, my_data->low, my_data->high);
        pthread_exit(NULL);
    }
    if (my_data->low < my_data->high){
        float pivot = my_data->vec[my_data->high];
        int i = (my_data->low - 1);
        float temp;
        for (int j = my_data->low; j <= my_data->high- 1; j++)
        {
            if (my_data->vec[j] < pivot)
            {
                i++;
                temp = my_data->vec[i];
                my_data->vec[i] = my_data->vec[j];
                my_data->vec[j] = temp;
            }
        }
        temp = my_data->vec[i+1];
        my_data->vec[i+1] = my_data->vec[my_data->high];
        my_data->vec[my_data->high] = temp;
        
        int p = i+1;
        
        thread_data thread_data_array[2];
        
        for (t = 0; t < 2; t ++)
        {
            thread_data_array[t].vec = my_data->vec;
            thread_data_array[t].thread_level = my_data->thread_level - 1;
        }
        thread_data_array[0].low = my_data->low;
        thread_data_array[0].high = p-1;
        thread_data_array[1].low = p+1;
        thread_data_array[1].high = my_data->high;

        pthread_t threads[2];
        for (t = 0; t < 2; t ++)
        {
            pthread_create(&threads[t], NULL, parallelQuicksort,&thread_data_array[t]);
        }
    
        for (t = 0; t < 2; t ++)
        {
            pthread_join(threads[t], NULL);
        }
    }
    
    pthread_exit(NULL);
}
